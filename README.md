# __tests_industrialisation_B3_EPSI_22_23__
### Project made in "Test & Industrialisation" classes during B3 at EPSI Rennes.
### Programmed by Aurélien GOURIOU 👾
### Exercice by Enzo Sandre : https://github.com/esandre

## 🐍 Python 3.10 with pytest

_Docstring of tests is in French to stick with the exercise._

**Exercises :**

| Step 1 | WHEN             | THEN                                   | STATUS |
|--------|------------------|----------------------------------------|--------|
| Test 1 | input string     | return mirrored string                 | ✔      |
| Test 2 | input palindrome | return mirrored string AND "Well said" | ✔      |
| Test 3 | input string     | return "Hello" before any response     | ✔      |
| Test 4 | input string     | "Goodbye" is sent last                 | ✔      |


| Step 2 | WHEN             | THEN                                                          | STATUS |
|--------|------------------|---------------------------------------------------------------|--------|
| Test 1 | input palindrome | return mirrored string AND "Well said" with the good language | ✔      |
| Test 2 | input palindrome | return mirrored string AND "Hello" with the good language     | ✔      |
| Test 3 | input string     | return mirrored string AND "Goodbye" with the good language   | ✔      |


| Step 3 | WHEN             | THEN                                                                                       | STATUS |
|--------|------------------|--------------------------------------------------------------------------------------------|--------|
| Test 1 | input palindrome | return mirrored string AND "Well said" with the good language and the good time of the day | ✔      |
| Test 2 | input palindrome | return mirrored string AND "Hello" with the good language and the good time of the day     | ✔      |

<u>__To run the project :__</u>
```cmd
git clone https://gitlab.com/Sullfurick/tests_industrialisation_epsi_b3_22_23.git

cd tests_industrialisation_epsi_b3_22_23

pip install -r requirements.txt

# For the tests
python -m pytest

# For the app
python main.py
```


<u>__TO-DO :__</u>
- Link system language and clock : DONE ✔
- Plug inputs to CLI : DONE ✔
- Line feed methods and tests : ❌
- Recipe test : ❌


<u>__TDD (Test Driven Development) methodology simplified :__</u>
```mermaid
graph LR 
w{WRITE TEST} -- run --> fail((Fail))

suc((success)) --> w

fail -- run --> f[WRITE FUNCTION]

f --> suc
     classDef blue fill:#533dfc,stroke:#333,stroke-width:2px
     classDef red fill:#c9140e,stroke:#333,stroke-width:1px
     classDef green fill:#26db02,stroke:#333,stroke-width:1px


     class w blue
     class f blue
     class fail red
     class suc green
```
