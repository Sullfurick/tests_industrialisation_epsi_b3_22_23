from src import Language


class OHCE:
    """
    OHCE utilisé pour l'exercice
    """

    def __init__(self, _language, _time_of_the_day):
        self.language = Language(_language, _time_of_the_day)
        # self.language.language_setter()

    def palindrome(self, word):
        """
        Fonction qui inverse un mot, qui dit "Bonjour", "Au revoir !" et qui détecte les palindromes et les félicitent !
        :param word: Le mot à inverser
        :return: result : Une chaine de caractères contenant "Bonjour !", "Au revoir !" ainsi que ", bien dit !" si
                          le mot est un palindrome
        """
        _reversed_word = self.reverse_word(word)
        result = self.hello() + _reversed_word + self.goodbye()
        if word == _reversed_word:
            result = self.hello() + word + self.well_said() + self.goodbye()
        return result

    @staticmethod
    def reverse_word(word_to_reverse):
        """
        Inverse les lettres d'un mot et le renvoie
        :param word_to_reverse:
        :return: reversed_word
        """
        return word_to_reverse[::-1]

    def hello(self):
        return self.language.hello

    def well_said(self):
        return self.language.well_said

    def goodbye(self):
        return self.language.goodbye
