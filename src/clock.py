

class Clock:
    """
    Cette classe agit comme une horloge fictive
    """

    def __init__(self, _time_of_the_day):
        self.time = 0
        self.time_of_the_day = _time_of_the_day
        self.select_time_of_the_day()

    def select_time_of_the_day(self):
        """
        Définit l'heure en fonction du moment de la journée
        """
        if self.time_of_the_day == "morning":
            self.time = 8
        elif self.time_of_the_day == "afternoon":
            self.time = 14
        if self.time_of_the_day == "evening":
            self.time = 20
        if self.time_of_the_day == "night":
            self.time = 4
