from src import SystemClock


class LanguageUser:
    """
    La classe LanguageUser est utilisé pour paramétrer la langue de l'OHCE en fonction de la langue du système
    """

    def __init__(self, _language):
        self.language = _language
        self.time = SystemClock().time
        self.hello = ""
        self.well_said = ""
        self.goodbye = ""
        self.language_dict = {
            'fr_FR': {
                "_hello": {
                    "_morning": "Bonjour ! ",
                    "_afternoon": "Bon aprem ! ",
                    "_evening": "Bonsoir ! ",
                    "_night": "Pas encore couché ? ",
                },
                "_well_said": ", bien dit !",
                "_goodbye": {
                    "_morning": " Passez une bonne matinée ! ",
                    "_afternoon": " Passez une bonne journée ! ",
                    "_evening": " Passez une bonne soirée ! ",
                    "_night": " Maintenant, au lit ! ",
                },
            },
            'en_US': {
                "_hello": {
                    "_morning": "Good morning ! ",
                    "_afternoon": "Good afternoon ! ",
                    "_evening": "Good evening ! ",
                    "_night": "You should be in bed right now... ",
                },
                "_well_said": ", well said !",
                "_goodbye": {
                    "_morning": " Have a nice morning !",
                    "_afternoon": " Goodbye !",
                    "_evening": " Sleep well !",
                    "_night": " Go to bed now !",
                },
            }
        }
        self.language_setter()

    def language_setter(self):
        """
        Cette fonction sert à paramétrer les attributs en fonction de la langue
        """
        if self.language == 'fr_FR':
            self.set_hello_and_goodbye()
            self.well_said = self.language_dict['fr_FR']['_well_said']
        elif self.language == 'en_US':
            self.set_hello_and_goodbye()
            self.well_said = self.language_dict['en_US']['_well_said']

    def set_hello_and_goodbye(self):
        """
        Cette fonction attribue une chaine de caractères à self.hello et self.goodbye en fonction du moment
         de la journée
        """
        if self.time < 6:
            self.hello = self.language_dict[self.language]['_hello']['_night']
            self.goodbye = self.language_dict[self.language]['_goodbye']['_night']

        elif 6 <= self.time <= 12:
            self.hello = self.language_dict[self.language]['_hello']['_morning']
            self.goodbye = self.language_dict[self.language]['_goodbye']['_morning']

        elif 12 < self.time <= 18:
            self.hello = self.language_dict[self.language]['_hello']['_afternoon']
            self.goodbye = self.language_dict[self.language]['_goodbye']['_afternoon']

        elif self.time > 18:
            self.hello = self.language_dict[self.language]['_hello']['_evening']
            self.goodbye = self.language_dict[self.language]['_goodbye']['_evening']
