import locale

from src import SystemClock
from src import LanguageUser


class OHCEUser:
    """
    OHCE branché à la console
    """

    def __init__(self):
        self.system_language = locale.getdefaultlocale()[0]
        self.system_time = SystemClock()
        self.language = LanguageUser(self.system_language)

    def palindrome_with_user_input(self):
        """
        Fonction qui inverse un mot, qui dit "Bonjour", "Au revoir !" et qui détecte les palindromes et les félicitent !
        :return: result : Une chaine de caractères contenant "Bonjour !", "Au revoir !" ainsi que ", bien dit !" si
                          le mot est un palindrome
        """
        word = self.user_input()
        _reversed_word = self.reverse_word(word)
        result = self.hello() + _reversed_word + self.goodbye()
        if word == _reversed_word:
            result = self.hello() + word + self.well_said() + self.goodbye()
        print(result)
        return result

    def user_input(self):
        _prompt = ""
        if self.language == 'fr':
            _prompt = input("Entrez votre mot : ")
        else:
            _prompt = input("Enter your word : ")
        return _prompt

    @staticmethod
    def reverse_word(word_to_reverse):
        """
        Inverse les lettres d'un mot et le renvoie
        :param word_to_reverse:
        :return: reversed_word
        """
        return word_to_reverse[::-1]

    def hello(self):
        return self.language.hello

    def well_said(self):
        return self.language.well_said

    def goodbye(self):
        return self.language.goodbye
