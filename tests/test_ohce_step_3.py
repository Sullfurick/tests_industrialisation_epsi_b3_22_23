import pytest

from src import OHCE


@pytest.mark.parametrize("language", ['fr', 'en'])
@pytest.mark.parametrize("time_of_the_day", ["morning", "afternoon", "evening", "night"])
# @pytest.mark.parametrize("time_of_the_day", [hour for hour in range(0, 24, 2)])
def test_ohce_hello_language_time(language, time_of_the_day):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est "période"
    QUAND on saisit une chaîne
    ALORS "Bonjour !" de cette langue à cette période est envoyé avant tout
    """
    ohce = OHCE(language, time_of_the_day)
    word = ohce.palindrome("logitech")
    assert word.__contains__("hcetigol")
    assert word.startswith(ohce.language.hello)


@pytest.mark.parametrize("language", ['fr', 'en'])
@pytest.mark.parametrize("time_of_the_day", ["morning", "afternoon", "evening", "night"])
def test_ohce_goodbye_language_time(language, time_of_the_day):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    ET que la période de la journée est "période"
    QUAND on saisit une chaîne
    ALORS "Au revoir !" de cette langue à cette période est envoyé avant tout
    """
    ohce = OHCE(language, time_of_the_day)
    word = ohce.palindrome("bel'veth")
    assert word.__contains__("htev'leb")
    assert word.endswith(ohce.language.goodbye)
