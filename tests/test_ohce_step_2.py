import pytest

from src import OHCE


# STEP 2
@pytest.mark.parametrize("language", ['fr', 'en'])
def test_ohce_well_said_language(language):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on entre un palindrome
    ALORS il est renvoyé
    ET le "bien dit !" de cette langue est envoyé
    """
    ohce = OHCE(language, "morning")
    word = ohce.palindrome("kayak")
    assert word.__contains__("kayak")
    assert word.__contains__(ohce.language.well_said)


@pytest.mark.parametrize("language", ['fr', 'en'])
def test_ohce_hello_language(language):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on saisit une chaîne
    ALORS "Bonjour !" de cette langue est envoyé avant tout
    """
    ohce = OHCE(language, "morning")
    word = ohce.palindrome("ibanez")
    assert word.__contains__("zenabi")
    assert word.startswith(ohce.language.hello)


@pytest.mark.parametrize("language", ['fr', 'en'])
def test_ohce_goodbye_language(language):
    """
    ÉTANT DONNE un utilisateur parlant une langue
    QUAND on saisit une chaîne
    ALORS "Au revoir !" dans cette langue est envoyé en dernier
    """
    ohce = OHCE(language, "morning")
    word = ohce.palindrome("ibanez")
    assert word.__contains__("zenabi")
    assert word.endswith(ohce.language.goodbye)
