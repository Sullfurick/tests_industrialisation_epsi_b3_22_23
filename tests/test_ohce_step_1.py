import pytest

from src import OHCE


# STEP 1
def test_ohce_mirror():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS celle-ci est renvoyée en miroir
    """
    ohce = OHCE('fr', "morning")
    word_to_reverse = "palindrome"
    assert ohce.palindrome(word_to_reverse).__contains__("emordnilap")


def test_ohce_palindrome():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit un palindrome
    ALORS celui-ci est renvoyé
    ET « Bien dit » est envoyé ensuite
    """
    ohce = OHCE('fr', "morning")
    word_to_reverse = "kayak"
    assert ohce.palindrome(word_to_reverse).__contains__("kayak, bien dit !")


def test_ohce_bonjour():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS « Bonjour » est envoyé avant toute réponse
    """
    ohce = OHCE('fr', "morning")
    word_to_reverse = str()
    assert ohce.palindrome(word_to_reverse).startswith("Bonjour !")


def test_ohce_aurevoir():
    """
    ÉTANT DONNÉ OHCE
    QUAND on saisit une chaîne
    ALORS « Au revoir » est envoyé en dernier.
    """
    ohce = OHCE('fr', "morning")
    word_to_reverse = str()
    assert ohce.palindrome(word_to_reverse).endswith("Passez une bonne matinée ! ")
